# 🗺 Atlas Logistique - Signal - Resources

This documentation explains how to administrate the SIGNAL Dashboard.

## 🌍 How to **add a new country** ? [(doc)](docs/pages/1_new_country.md)

## 📂 How to **update the catalog** ? [(doc)](docs/pages/2_catalog.md)