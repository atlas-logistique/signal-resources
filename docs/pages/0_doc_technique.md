# GUIDE IM SIGNAL

## ARCHITECTURE DES DONNEES
### Répertoire racine
Par défaut le répertoire de données est nommé "data" et placé dans le répertoire racone des scripts R.

### Répertoires enfants
Chaque répertoire de données contenant les informations necessaires au calcul doivent être nommés de cette façon :
- "01_Boundaries"
- "02_Roads"
- "03_Gateways"
- "04_Chief_towns"
- "05_Population"
- "06_DHS"
- "07_Obstacles"
- "08_Elevation"
- "09_Markets"

### Couches de données
Par défaut les données géographiques sont au format shape ou les vecteurs et tif pour les rasters. Il peut y avoir des données en CSV ou pour les données DHS le format ".SAV" est préféré.
Par défaut les couches sont nommées ainsi :
1. Limites administratives :
    - "ADM1.shp"
    - "ADM2.shp"
    - "ADM3.shp"
2. Routes :
    - "roads.shp"
    - "road_network.shp"
3. Portes internationales :
    - "gateways.shp"
4. Chef lieux :
    - "chief_town_adm1.shp"
    - "chief_town_adm2.shp"
    - "chief_town_adm3.shp"
5. Population :
    - *cf fichier de configuration*
6. DHS :
    - *cf fichier de configuration*
7. Obstacles :
    - "obstacles.shp"
8. Elevation :
    - *cf fichier de configuration*
9. Marchés :
    - "markets.shp"

### Fichier de configuration
Si le travail de mise en forme est données est préalablement réalisé vous ne devriez pas avoir besoin de modifier autre chose que le fichier **settings.R**.
Dans **settings.R** vous aurez accès aux variables suivantes :
- **".WORKING_DIRECTORY"** : Il s'agit du répertoire dans lequel sont vos scripts R et votre répertoire de données. 
- ".DATA_DIRECTORY" : Ce dernier est la concaténation de votre répertoire de travail + le répertoire "data" dans lequel devraient se trouver vos données. Si vous avez respecté la configuration par défaut cette variable ne necessite pas de modification.
- **".EPSG_COUNTRY"** : La projection sur laquelle vous travaillez. Il est conseillé d'utiliser une projection métrique connue des bibliothèques R et qui limite la déformation entre la conversion WSG84 et la projection de travail.
- **".DHS_DATA_DIR"** : Sous répertoire pour les données DHS. Il s'agit du répertoire des données au format CSV, ASCII ou SAV. Ce répertoire est un nom fourni par le programme.
- **".DHS_DATA_FILENAME"** : Nom du fichier de données avec son extension (ex : "MZHR81FL.SAV"). Il s'agit là encore d'un nom donné par le programme contenant les informations relatives au pays étudié, à l'année de référence etc.
- **".DHS_CLUSTER_DIR"** : Sous répertoire des données géographiques DHS contenant les clusters au format shape.
- **".DHS_CLUSTER_FILENAME"** : Nom du fichier de données géographiques. 
- **".fields_dhs_motorized"** : Nom des colonnes de travail utilisées pour travailler sur le fichier ".DHS_DATA_FILENAME". Il est important de se référer à la documentation de l'année du programme afin de s'assurer que ces champs sont toujorus existants et qu'ils pointent sur la donnée qui nous interesse :
    - "HHID" : Identifiant des foyers
    - "HV001" : Identifiant du cluster
    - "HV002" : Identifiant du foyer au sein du cluster
    - "HV210" : Le foyer possède un vélo
    - "HV211" : Le foyer possède un deux roues motorisé
    - "HV212" : Le foyer possède une voiture
    Ces données sont accessibles sur les documents DHS Recode. Attention cependant à la version du programme, la dernière en date : https://dhsprogram.com/pubs/pdf/DHSG4/Recode7_DHS_10Sep2018_DHSG4.pdf
- **".POP_RASTER_FILENAME"** : Nom de la couche de population téléchargée sur WorldPop. Le nom change en fonction du pays et des choix de méthodologie. Il était donc interessant de le laisser ainsi.
- **".ELEVATION_RASTER_FILENAME"** : Nom de la couche d'élévation. Par défaut il s'agit d'un SRTM mais nous pouvons parfois utiliser les données fournies par le pays si plus précisent ou plus pertinentes.

### Arborescence des données
Repertoire du script :\
|_  data\
&emsp;|_  01_Boundaries\
&emsp;&emsp;|_  ADM1.shp\
&emsp;&emsp;|_  ADM2.shp\
&emsp;&emsp;|_  ADM3.shp\
&emsp;|_  02_Roads\
&emsp;&emsp;|_  roads.shp\
&emsp;&emsp;|_  road_network.shp\
&emsp;|_  03_Gateways\
&emsp;&emsp;|_  gateways.shp\
&emsp;|_  04_Chief_towns\
&emsp;&emsp;|_  chief_town_adm1.shp\
&emsp;&emsp;|_  chief_town_adm2.shp\
&emsp;&emsp;|_  chief_town_adm3.shp\
&emsp;|_  05_Population\
&emsp;&emsp;|_  moz_ppp_2020_UNadj.tif\
&emsp;|_  06_DHS\
&emsp;&emsp;|_  MZHR81SV\
&emsp;&emsp;&emsp;|_  MZHR81FL.SAV\
&emsp;&emsp;|_  MZGE81FL\
&emsp;&emsp;&emsp;|_  MZGE81FL.shp\
&emsp;|_  07_Obstacles\
&emsp;&emsp;|_  obstacles.shp\
&emsp;|_  08_Elevation\
&emsp;&emsp;|_  srtm_ninety.tif\
&emsp;|_  09_Markets\
&emsp;&emsp;|_  markets.shp


## FORMAT DES DONNEES
### Considérations générales
1. **Projection** : Idéalement vous aurez reprojeté les couches vectorielles et vérifié visuellement leur bonne superposition. Une reprojection se fait à l'ouverture des fichiers par le script R mais pour limiter les erreurs il est conseillé de l'avoir effectué en amont.
2. Le champ **OID/ID** n'est pas présent car il va de soi qu'une couche vectorielle doit avoir un champ d'identifiant unique. Certaines couches auront ce champ d'identification propre comme ADMx_PCODE pour les limites administratives ou HHID pour les données DHS.

### Limites administratives
Il faut garder en tête que le fonctionnement du dashboard utilise les PCODE du format OCHA. Il faudra créer la colonne ADMx_PCODE et la mettre à jour dans le cas où le calcul est effectué avec une couche de référenciel national.

1. Champs minimaux ADM1.shp

| ADM1_EN   | ADM1_PCODE    | GEOMETRY  |
| --- | --- | --- |
| String    | String        | Polygon   |

2. Champs minimaux ADM2.shp

| ADM2_EN   | ADM2_PCODE    | ADM1_EN   | ADM1_PCODE    | GEOMETRY  |
|-----------|---------------|-----------|---------------|-----------|
| String    | String        | String    | String        | Polygon   |

3. Champs minimaux ADM3.shp

| ADM3_EN   | ADM3_PCODE    | ADM2_EN   | ADM2_PCODE    | ADM1_EN   | ADM1_PCODE    | GEOMETRY  |
|-----------|---------------|-----------|---------------|-----------|---------------|-----------|
| String    | String        | String    | String        | String    | String        | Polygon   |

### Routes
Une seule couche de routes est necessaire au calcul de l'IVL. Cependant la création en amont d'une couche accrochée et fusionnée 'road_network" permettra d'augmenter la vitesse de calcul et surtout de limiter les erreurs.

1. Champs minimaux roads.shp

| STA_GOOD  | STA_FAIR  | STA_POOR  | GEOMETRY  |
|-----------|-----------|-----------|-----------|
| Integer   | Integer   | Integer   | Polyline  |

STA_GOOD représente le pourcentage du tronçon en bon état\
STA_FAIR représente le pourcentage du tronçon en état moyen\
STA_POOR représente le pourcentage du tronçon en mauvais état\


Avec une excellente granularité il est possible de mettre des valeurs dans plusieurs champs pour un seul tronçon. Attention cependant, la somme de chaque ne doit pas dépasser 100%\
Avec une information correcte chaque tronçon sera en 100% dans une des trois colonnes (route en bon, moyen ou mauvais état)

2. Champs minimaux road_network.shp

| GEOMETRY  |
|-----------|
| Polyline  |

Il n'y a pas de champs minimaux requis, le calcul se fera sur la longueur des entités.\
Il s'agit ici d'avoir une couche topologiquement juste pour les calculs nécessitant le passage par le réseau, notamment l'indicateur GT.

Pour se faire nous allons :
- reprojeter la couche dans le bon référentiel
- corriger les éventuelles erreurs topologiques
- snapper les portes internationales
- snapper les chefs-lieux
- snapper les extrémités des routes. Attention cette étape peut avoir des effets indésirables dans le cas d'un référentiel précis avec plusieurs routes parallèles.
- fusionner l'ensemble après avoir vérifié que nous n'avons pas introduit d'erreurs topologiques

### Portes internationales
La couche de portes internationales ne nécessite aucune information, les points seront le paramètre de destination dans le calcul de l'indicateur GT.

1. Champs minimaux gateways.shp

| GEOMETRY  |
|-----------|
| Point     |

### Chef lieux
Les points des couches de chef-lieux seront le paramètre de départ dans le calcul de l'indicateur GT.\
Chaque échelle administrative nécessite sa couche car il doit y avoir un chef-lieu par unité administrative. Selon les pays, certains chefs-lieux ne sont pas conservés d'une échelle administrative à une autre.

1. Champs minimaux chief_town_adm1.shp

| ADM1_PCODE    | GEOMETRY  |  
|---------------|-----------|  
| String        | Point     |  

2. Champs minimaux chief_town_adm2.shp

| ADM2_PCODE    | ADM1_PCODE    | GEOMETRY  |
|---------------|---------------|-----------|
| String        | String        | Point     |

3. Champs minimaux chief_town_adm3.shp

| ADM3_PCODE    | ADM2_PCODE    | ADM1_PCODE    | GEOMETRY  |
|---------------|---------------|---------------|-----------|
| String        | String        | String        | Point     |

### Population
Il s'agit d'une couche raster, une reprojection préalable de la couche peut accélérer son chargement par le script R.

### Demographic and Health Surveys
Ces couches sont dans un format propre au programme DHS : https://dhsprogram.com/data/Data-Tools-and-Manuals.cfm
Le fichier de données (SAV) doit contenir les données suivantes :
- "HHID" : Identifiant des foyers
- "HV001" : Identifiant du cluster
- "HV002" : Identifiant du foyer au sein du cluster
- "HV210" : Le foyer possède un vélo
- "HV211" : Le foyer possède un deux roues motorisé
- "HV212" : Le foyer possède une voiture

Le fichier shape quant à lui contient les clusters qui seront joints par le champ "HV001"

### Obstacles
Aucune donnée particulière n'est néccessaire si ce n'est la géometrie. Ces obstacles sont des zones infranchissables pour un individu à pied (lac, glaciers, etc.)

1. Champs minimaux obstacles.shp

| GEOMETRY  |
|-----------|
| Polygon   |

### Elevation
Il s'agit d'une couche raster, une reprojection préalable de la couche peut accélérer son chargement par le script R.

### Markets
La couche des marchés est une couche de points permettant le calcul d'une zone d'accès en utilisant la couche d'élévation. Seule la géométrie est nécessaire.

1. Champs minimaux markets.shp

| GEOMETRY  |
|-----------|
| Point     |


## SCRIPTS
### Présentation générale
Repertoire du script\
|_  data\
|&emsp;|_ *\
|_  main.R\
|_  settings.R\
|_  sources.R\
|_  utils.R\
|_  RNI.R\
|_  RCI.R\
|_  GT.R\
|_  PA.R\
|_  create_market_layer.R\
|_  SCAW.R\
|_  TR.R

### main.R
main.R est le script que vous voulez lancer et qui idéalement calculera l'entièreté des valeurs de l'IVL.\
Le script se compose des parties suivantes :
1. Appel des scources et Lib necessaires
2. Chargement des données
3. Calcul des indicateurs
4. Creation de l'IVL et sortie fichier

### settings.R
Contenu du fichier abordé au chapitre "Architecture des données" / "Fichier de configuration"

### sources.R
Vous ne devriez pas avoir à modifier ce fichier si le fichier de configuration est mis à jour correctement, l'arborescence du projet et le nommage des fichiers respecté.\
Ce fichier contient de simples variables de concatenation du type : .file_path_adm1 <- paste0(.DATA_DIRECTORY, .adm_path, "ADM1.shp") et des fonctions de chargement des fichiers shapes, rasters et SAV qui sont simplement appelées dans le fichier "main.R" (exemple : a2 <- load_adm_boundaries("ADM2"))

### utils.R
Ce script contient des fonctions utilisées dans le main.R ou d'autres comme la fonction format_var_for_subindicator qui permet de fonctionaliser les paramètres en fonction de l'appel de différent indicateurs

### create_market_layer.R
Ce script permet de générer une couche des marchés depuis la couche de population lorsque la donnée est manquante.
Chaque fonction du script a une docstring explicitant son travail.

### fichiers indicateurs (RNI, RCI, GT, PA, SCAW, TR)
Calcul des indicateurs fonctionnalisés. Faute de temps, certains sont mieux documentés/commentés que d'autres


## METHODOLOGIE
Utilisation générale du script et détails sur les indicateurs

### RNI
Données nécessaires : 
- Limites administratives
- Réseau routier


Intersection des routes par les limites administratives et calcul de la longueur du réseau routier pour chaque entité administrative.

### RCI
Données nécessaires : 
- Limites administratives
- Réseau routier


Intersection des routes par les limites administratives et calcul de la longueur du réseau routier en bon et moyen état sur la longueur totale par limite administrative.

### GT
Données nécessaires : 
- Limites administratives
- Réseau routier
- Portes internationales
- Chefs lieux (différents niveaux administratifs)


Cet indicateur va calculer la distance qui relie chaque chef lieu à la porte internationale la plus proche tout en utilisant exclusivement le réseau routier.\
/!\ Le réseau doit être topologiquement juste. Il est conseillé pour éviter les erreurs de calcul de faire plusieurs étape préliminaires manuellement :
- Accrochage des portes internationales et chefs lieux au réseau avec un buffer de 5km
- Accrochage des routes dans le cas ou les extrémités sont parfois disjointes
- Identification et réparation des topologies pouvant poser problèmes
- Fusion des entités afin de ne former qu'une, le calcul se fera sur la longueur des segments utilisés


C'est pour cela que nous pourrez trouver une couche 'road_network' en plus de la couche de routes 'roads'\
Pensez à visualiser les résultats pour repérer les éventuelles anomalies au niveau ADM1 et ADM2

### PA
Données nécessaires : 
- Limites administratives
- Réseau routier
- Raster de population


Indicateur non encore fonctionalisé car il se base sur la donnée la plus fine (ADM3 ou ADM2) afin de calculer l'échelle suivante.\
Calcul de la population par zone administrative, creation d'un buffer de 5km autour du réseau routier et pour chaque unité administrative calcul du pourcentage de la population dans ce buffer.

### MI
Données nécessaires : 
- Limites administratives
- Données DHS


Simple rapport foyers motorisés ou non. Cet indicateur se base uniquement sur les données DHS.

### SCAW
/!\ Des impossibilités de calculs ont été levées si une version suppérieure à la version 3.16 de QGIS était utilisée. Il faudra fiabiliser son utilisation avec les versions les plus récentes de l'algorithme de calcul pédestre.\
Données nécessaires : 
- Limites administratives
- Marchés
- Raster de population
- Modèle Numérique de Terrain


Cet indicateur calcule le pourcentage de population à proximité d'un marché pour chaque zone administrative.

Plusieurs étapes peuvent nécessiter leur execution manuelle :
- la création des marchés :
    - La première méthode utilise le script "create_market_layer.R" qui va générer un point pour chaque zone administrative la plus précise (généralement ADM3) en fonction de la couche de population. Il est estimé que le pixel ayant la plus forte densité humaine est potentiellement une zone de marché.
    - Utiliser directment la couche des chefs lieux ADM3.
    Les deux méthodes sont imparfaites car il peut y avoir plusieurs marchés même au niveau ADM3
- le calcul du coup de déplacement
    - Le calcul du coup de déplacement se fait pour chaque marché avec le modèle numérique de terrain et peut être très long. En cas d'erreur l'intégralité du calcul est perdu. Il peut être utile de calculer des portions en utilisant un slice et de fusionner les points après. Dans le cas ou les erreurs ne peuvent pas être corrigées l'utilisation pontuelle d'un MNT avec une précision plus faible peut être une solution.

### TR
Données nécessaires : 
- Limites administratives
- Modèle Numérique de Terrain


L'indicateur TR est un simple calcul de rugosité qui se fait via l'import du script de calcul de la rugosité de QGIS