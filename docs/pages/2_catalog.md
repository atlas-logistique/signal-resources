## 📂 How to **update the catalog** ? 

Catalog is separated into two parts :

1. The [catalog folder repository](/catalog) (hosted on Giltab)
2. The [catalog metadata file](https://docs.google.com/spreadsheets/d/1mS-AhVWxB0bOjx-RJ0Q9tplqQxPuLFBeB2QDv6sEjEU/edit#gid=0) (in a GoogleSheet)

### How to add a document ?

#### **Requirements**
- Have Write permission to the Gitlab [`SIGNAL Resources`](https://gitlab.com/atlas-logistique/signal-resources) repository
- Have edit permissions to the [catalog metadata file](https://docs.google.com/spreadsheets/d/1mS-AhVWxB0bOjx-RJ0Q9tplqQxPuLFBeB2QDv6sEjEU/edit#gid=0)

#### **How to ?**
1. Access the [`SIGNAL Resources`](https://gitlab.com/atlas-logistique/signal-resources) repository
2. Go to the `catalog` folder
3. Upload the document by clicking on th `+` button next to the breadcrumbs and click `Upload file`

![Image](/docs/resources/upload_file.png?raw=true "Title"){height=150px}

4. In the popup drop your file and press `Upload`
5. Once done you should be redirected to your uploaded file page
6. Right-click on the download button, and copy the link

![Image](/docs/resources/get_document_link.png?raw=true "Title"){height=200px}

7. Access to the [catalog metadata file](https://docs.google.com/spreadsheets/d/1mS-AhVWxB0bOjx-RJ0Q9tplqQxPuLFBeB2QDv6sEjEU/edit#gid=0)
8. In a new row paste the link you just copied in the `link` column
9. Remove `?inline=false` at the end of the url
10. You can complete the metadata info :

|             | Description                                                                  | Multiple | Options                                                                              | Example                                                                                      |
|-------------|------------------------------------------------------------------------------|----------|--------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|
| `name_EN`   | **English name of the document**                                             |          |                                                                                      | Burkina Faso (Seno) - Evaluation Summary Sheet                                               |
| `name_FR`   | **French name of the document**                                              |          |                                                                                      | Burkina Faso (Séno) - Fiche récapitulative d'évaluation                                      |
| `action`    | **Way the ressource is supposed to perform on click**                        |          | `open`\|`download`\|`request_access` | open                                                                                         |
| `format`    | **Format of the document**                                                   |          | `pdf`\|`word`\|`xls`                                                                 | pdf                                                                                          |
| `link`      | **Link of the document**                                                     |          |                                                                                      | https://gitlab.com/atlas-logistique/signal-resources/-/raw/main/1.FICHE_RECAP_SENO_BF_VF.pdf |
| `countries` | **Countries the ressource is linked to, following ISO3 format in uppercase** | yes      |                                                                                      | BFA                                                                                          |
| `admin1`    | **Admin 1 the ressource is linked to, following PCode format in uppercase**  | yes      |                                                                                      | BF56                                                                                         |
| `admin2`    | **Admin 2 the ressource is linked to, following PCode format in uppercase**  | yes      |                                                                                      | BF5602                                                                                       |
| `thematic`  | **Thematic**                                                                 | yes      |                                                                                      | methodologie                                                                                 |
| `years`     | **Year source of the resource**                                              | yes      |                                                                                      | 2021                                                                                         |