## 🌍 How to **add a new country** ?

### _Note_

**NB :** Everytime, a country iso3 is needed, **always** put it in lower case.\
_**ex :** For Burkina Faso, `bfa` is correct, but `BFA` isn't_

### **#01 - Add the country to the [Country config file](https://docs.google.com/spreadsheets/d/18pAvN9Pg0AoGN-1_PRVpKEr61BRTUgG7yOVBgazecvs/edit#gid=1598067983)**

### **#02 - Upload LVI data :**

Upload the indicators data as CSV files following this path : `data/lvi/<iso3>/adm<1-3>.csv`

_**ex:**_

- `data/lvi/hti/adm1.csv`
- `data/lvi/hti/adm2.csv`
- `data/lvi/hti/adm3.csv`

### **#03 - Add Boundaries :**

Here are the rules :

- **3 different admin level** source files (1, 2 and 3) (if there's no admin 3 for a country for example, you don't need to upload it)
- Format needs to be **`.topojson`** (for lightweight sake)
- Features needs to be set with **pcodes** properties (Download **OCHA** source)
  - `ADM1_PCODE` for Admin 1
  - `ADM2_PCODE` for Admin 2
  - `ADM3_PCODE` for Admin 3
- Source must be **simplified** as much as possible without quality loss.

To answer these rules here is the process to follow :

📺 _**You can follow [this video tutorial](docs/resources/export_and_minimize_topojson.mp4), describing the next following steps :**_

1. a) Download a the country boundaries shapefile from [HDX](https://data.humdata.org/dataset/) using this [preset search](<https://data.humdata.org/dataset?ext_administrative_divisions=1&res_format=SHP&q=&sort=if(gt(last_modified%2Creview_date)%2Clast_modified%2Creview_date)%20desc&ext_page_size=25>).

- **Source should be `OCHA`** to have the pcode structure
- Data should **contain the wanted admin level boundaries**
- Data format should be downloadable to shapefile `SHP` or GeoJson `GEOJSON` or Topojson `TOPOJSON`

1. b) Alternatively, you can go to [https://fieldmaps.io/data/cod](https://fieldmaps.io/data/cod) :\
It's based on data found on HDX referencing OCHA sources\
(ADM column indicates the admin level granularity available, **download Original SHP**)

2. Go to **[mapshaper.org](https://mapshaper.org)**
3. **Drag and drop your downloaded `shp.zip`** and press **import** button
4. **Delete unneeded layers** to only keep admin 1, 2 and 3 (admin0 is not needed) delete all other ones.

5. As all sources downloaded from HDX can have different topology and the Pcode attribute name could change from one source to the other (even from OCHA), we need to retrieve the pcode key name before executing the simplify command.

- Step 1 - **Select "Edit attribute"** Mapshaper feature, by clicking on the cursor icon button (below map zoom buttons). Click on "Edit attributes"
- Step 2 - **Select a random feature** on the map
- Step 3 - **Check the pcode key** of the admin level you selected. Write it down somewhere, this is `<old_pcode_property>` You will need it afterwards.

![Image](./docs/resources/mapshaper_inspect.PNG?raw=true "Title"){height=300px}

5. Now **simplify each layers** without shape removal :
For this, open the Console in the right side of the navigation bar.

Copy-paste the following command and adjust it with the admin level and the old_pcode_property.

Adjust `<admin_level>` and `<old_pcode_property>` to your needs.

```bash
mapshaper -simplify 13% keep-shapes visvalingam -each 'this.properties={ADM<admin_level>_PCODE:this.properties.<old_pcode_property>}' -o format="topojson" "adm<admin_level>.json"
```

_**ex:** For South Sudan (Admin 3) :_

```bash
mapshaper -simplify 13% keep-shapes visvalingam -each 'this.properties={ADM3_PCODE:this.properties.adm3Pcode}' -o format="topojson" "adm3.json"
```

**NB :** You can adjust the simplify percentage from 13% to another to have the minimum loss of quality.\

Press **Enter**.

If all is ok, the topojson should be downloaded.

### **#04 - Upload Topojson data :**

Upload the exported topojsons in the Signal Resources repository following this path : `data/geo/<iso3>/adm<1-3>.json`

_**ex :**_

- `data/geo/hti/adm1.json`
- `data/geo/hti/adm2.json`
- `data/geo/hti/adm3.json`

### **#05 - Add the metadata :**

Add metadata to the [`data/geo_layers.json`](https://gitlab.com/atlas-logistique/signal-resources/-/blob/main/data/geo_layers.json?ref_type=heads) file

```json
[
  ...
  // Add these lines at the end of the array
  {
    "id": "<iso3>",
    "iso": "<iso3>",
    "adminSources": {
      "1": "/data/<iso3>/boundaries/<iso3>_adm1.json",
      "2": "/data/<iso3>/boundaries/<iso3>_adm2.json",
      "3": "/data/<iso3>/boundaries/<iso3>_adm3.json"
    },
    "hoveredId": null,
    "activeId": null,
    "features": { 1: null, 2: null, 3: null }
  }
]
```

### **5. Important step - Check everything is working fine**

Check in the app that all is fine.
